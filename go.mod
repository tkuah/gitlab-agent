module gitlab.com/ash2k/gitlab-agent

go 1.14

require (
	github.com/ash2k/stager v0.1.0
	github.com/golang/protobuf v1.4.1
	github.com/stretchr/testify v1.5.1
	golang.org/x/net v0.0.0-20200506145744-7e3656a0809f // indirect
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
	golang.org/x/sys v0.0.0-20200509044756-6aff5f38e54f // indirect
	golang.org/x/tools v0.0.0-20200507205054-480da3ebd79c
	google.golang.org/genproto v0.0.0-20200507105951-43844f6eee31 // indirect
	google.golang.org/grpc v1.29.1
	k8s.io/client-go v0.18.2
)
